
public class Const {
	Const()
	{
		System.out.println("Default constructor");
	}
	Const(String s)
	{
		this();
		System.out.println(s);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Const("Parameter Constructor");
	}

}
